﻿namespace Knapsack.Attributes {
	public static class IDamageableHelper {
		public static void OnDestroyedDefault(this IDamageable damageable,Data data) {
			data.Stack.Nullify();
		}
		public static void DamageDefault(this IDamageable damageable,Data data,int amount) {
			PropertyHolder holder = data.Stack.Properties;
			System.Console.WriteLine("Slot: " + data.Slot);
			int durability = holder.Read<int>("durability");
			durability -= amount;
			holder.Write("durability",durability);
			if (durability <= 0) {
				damageable.OnDestroyed(data);
			}
		}
	}
}
