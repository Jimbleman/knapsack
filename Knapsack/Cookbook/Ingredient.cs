﻿using System;
using Knapsack.Items;
namespace Knapsack.Cookbook {
	public class Ingredient<T>: Ingredient where T : BaseItem {
		int Amount;
		bool Strict;
		public Ingredient(int amount, bool strict = true) {
			Amount = amount;
			Strict = strict;
		}

		public bool Valid(ItemStack stack) {
			Type itemType = stack.ItemType.GetType();
			return itemType is T || (!Strict && itemType.IsSubclassOf(typeof(T)));
		}
	}
	public abstract class Ingredient {}
}
