﻿using Knapsack.Items;
using System;

namespace Knapsack.Cookbook {
	public class Recipe {
		Ingredient[] Ingredients;
		BaseItem Output;
		int OutputCount;
		public Recipe(BaseItem output, int outputCount, bool strict, params Ingredient[] ingredients) {
			Ingredients = ingredients;
			Output = output;
			OutputCount = outputCount;
		}

		public ItemStack Craft(ItemStack[] items, int amount) {
			// dummy itemstack of output type
			// for amount times (or one ingredient runs out):
			//     craft once
			//     merge single craft with dummy
			// return dummy
			throw new NotImplementedException();
		}

		public ItemStack Craft(ItemStack[] items) {
			// check if all items are valid
			// check if there are sufficient amounts of all ingredients
			// shrink all stacks by the amount needed of each
			// return a new itemstack of type output with output amount
			throw new NotImplementedException();
		}
	}
}
