﻿namespace Knapsack.Items {
	public class Items {
		// base items
		public static ItemGasdad		Gasdad = new ItemGasdad();
		public static ItemNotGasdad		NotGasdad = new ItemNotGasdad();
			// actionables
			public static ItemScroll		Scroll = new ItemScroll();
			public static ItemBinoculars	Binoculars = new ItemBinoculars();

		// damageables
		public static ItemCottonFluff	Fluff = new ItemCottonFluff();

		// equipment
		public static ItemStetson		Stetson = new ItemStetson();
		public static ItemMinerHelmet	MinerHelmet = new ItemMinerHelmet();
	}
}
