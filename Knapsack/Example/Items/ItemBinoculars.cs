﻿using Knapsack.Items;
using Knapsack.Attributes;
using Knapsack;
using System;
using System.Collections.Generic;

public class ItemBinoculars : BaseActionable {
	private string[] scenes;

    public ItemBinoculars() {
		actions.Add("peer",OnPeer);
		actions.Add("shatter",OnShatter);

		scenes = new string[] {
			"a graveyard, with a congregation of goths having a party",
			"a large mass of spiders travelling at a worrying speed",
			"several medium-sized vans parked next to dumpster",
			"a billboard with your face printed on it"
		};
	}

	public override string Name {
		get {
			return "Binoculars";
		}
	}

	public override string Description {
		get {
			return "A reusable pair of binoculars. If you looked through this on a hill\nyou could probably see a lot of interesting things.";
		}
	}

	public override int StackSize {
		get {
			return 1;
		}
	}

	private string Scenery {
		get {
			return scenes[Random.Next(0,scenes.Length)];
		}
	}

	protected void OnPeer(Data data) {
		Console.WriteLine(string.Format("You peer through the binoculars and see {0}.",Scenery));
	}

	protected void OnShatter(Data data) {
		Console.WriteLine("You shatter the binoculars in frustration, throwing shards of glass everywhere. Ow.");
		data.Stack.Remove(1);
	}
}
