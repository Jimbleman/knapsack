﻿using System;
using Knapsack;
using Knapsack.Items;
using Knapsack.Attributes;

public class ItemCottonFluff : BaseItem, IDamageable {
	public ItemCottonFluff() : base() {
		Properties.Add("durability",MaxDurability);
	}

	public override string Name {
		get {
			return "Cotton Fluff";
		}
	}
	public override string Description {
		get {
			return "A quite large ball of fluff. You could probably pick off a lot of fibers before it disappears.";
		}
	}
	public int MaxDurability {
		get {
			return 10;
		}
	}
	public void Damage(Data data,int amount) {
		Console.WriteLine("You pick " + amount + " handfuls off the fluff.");
		this.DamageDefault(data,amount);
	}
	public void OnDestroyed(Data data) {
		Console.WriteLine("You look down at the fluff and realize you can't see it anymore.");
		this.OnDestroyedDefault(data);
	}
}