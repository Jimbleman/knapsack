﻿using Knapsack.Items;

public class ItemGasdad : BaseItem {
	public override string Name {
		get {
			return "Gas Dad";
		}
	}

	public override string Description {
		get {
			return "A father made of gas. Lightly lovable!";
		}
	}
}
