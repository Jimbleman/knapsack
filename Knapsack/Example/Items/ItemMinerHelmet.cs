﻿using System;
using Knapsack;
using Knapsack.Items;
using Knapsack.Attributes;
using System.Collections.Generic;

public class ItemMinerHelmet : BaseEquipment, IActionable {
	protected Dictionary<string,Action<Data>> actions;
	public Dictionary<string,Action<Data>> Actions {
		get {
			return actions;
		}
	}
	public ItemMinerHelmet() : base() {
		Properties.Add("on",false);

		actions = new Dictionary<string,Action<Data>>();
		actions.Add("switch",OnSwitch);
	}

	public override string Name {
		get {
			return "Miner's Helmet";
		}
	}
	public override string Description {
		get {
			return "It's bright and yellow, but it's not a banana. There's an enormous light on the front.";
		}
	}
	public override int MaxDurability {
		get {
			return 300;
		}
	}
	public override void OnEquip(Data data) {
		base.OnEquip(data);
		Console.WriteLine(string.Format("You slip on the {0}. It fits perfectly, almost like it was... created for you... Spooky.",Name));
	}
	public override void OnUnequip(Data data) {
		base.OnUnequip(data);
		Console.WriteLine(string.Format("You remove the {0}. This hat is freaky, and personally, I wouldn't trust it.",Name));
	}

	public override void Damage(Data data,int amount) {
		base.Damage(data,amount);
	}
	public override void OnDestroyed(Data data) {
		Console.WriteLine(string.Format("Your {0} fractures, and you can hear faint screams trickling out of the cracks.",Name));
		base.OnDestroyed(data);
	}

	public void Invoke(string key,Data data) {
		this.InvokeDefault(key,data);
	}

	protected void OnSwitch(Data data) {
		bool on = Properties.Read<bool>("on");
		on = !on;
		if(on) {
			Console.WriteLine("You switch on the light, and the area in front of you floods with a sickly yellow.");
		} else {
			Console.WriteLine("You switch off the light. Everything seems much less bright. Not metaphorically.");
		}
		Properties.Write<bool>("on",on);
	}

	public override string ToString() {
		return this.ToStringDefault(base.ToString());
	}
}
