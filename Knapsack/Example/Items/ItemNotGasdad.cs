﻿using Knapsack.Items;

public class ItemNotGasdad : BaseItem {
	public override string Name {
		get {
			return "Not Gas Dad";
		}
	}

	public override string Description {
		get {
			return "He never attended your baseball games.";
		}
	}
}
