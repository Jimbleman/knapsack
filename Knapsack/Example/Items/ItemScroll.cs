﻿/* 
 * A test item. Does nothing but burn your eyes.
 */
using System;
using System.Collections.Generic;
using Knapsack;
using Knapsack.Items;

public class ItemScroll : BaseActionable {
	public ItemScroll() {
		actions.Add("read",OnRead);
	}

	public override string Name {
		get {
			return "Mysterious magical scroll";
		}
	}

	public override string Description {
		get {
			return "A magical scroll covered in strange symbols.\nYou could probably decipher it if you sat down and looked closely.";
		}
	}

	public override int StackSize {
		get {
			return 10;
		}
	}

	protected void OnRead(Data data) {
		Console.WriteLine("As you read the scroll, you feel your eyes burning\nin their sockets from the disturbing content inscribed.");
		data.Stack.Remove(1);
	}
}