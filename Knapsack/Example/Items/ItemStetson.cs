﻿using System;
using Knapsack;
using Knapsack.Items;

public class ItemStetson : BaseEquipment {
	public override string Name {
		get {
			return "Stetson";
		}
	}
	public override string Description {
		get {
			return "A sturdy hat. It could probably hold up about\nthe weight of a car or maybe a small pony.";
		}
	}
	public override int MaxDurability {
		get {
			return 1000;
		}
	}
	public override void OnEquip(Data data) {
		base.OnEquip(data);
		Console.WriteLine(string.Format("The {0} presses down on your hair. It feels astoundingly heavy; how did you ever carry this before?", Name));
	}
	public override void OnUnequip(Data data) {
		base.OnUnequip(data);
		Console.WriteLine(string.Format("You remove the {0}, and it feels like a weight has been lifted off your shoulders. Er, head.", Name));
	}

	public override void Damage(Data data,int amount) {
		base.Damage(data,amount);
		
	}
	public override void OnDestroyed(Data data) {
		Console.WriteLine(string.Format("Your {0} dissolves into a thousand tiny pieces.", Name));
		base.OnDestroyed(data);
	}
}