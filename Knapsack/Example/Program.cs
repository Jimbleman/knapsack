﻿using System;
using System.Collections.Generic;
using Knapsack;
using Knapsack.Items;
using Knapsack.Attributes;

public class Program {
	public static bool shouldRun = true;
	public static Inventory inventory1; // storage inventory
	public static Inventory inventory2; // working inventory
	public static Inventory working;
	public static List<Inventory> invs;

	public static List<Command> commands;

	static void Main(string[] args) {
		InitCommands();

		inventory1 = new Inventory(0, true);
		inventory1.Expand<BaseEquipment>(1);
		inventory1.Expand(9);

		inventory2 = new Inventory(0, true);
		inventory2.Expand<BaseEquipment>(5);

		invs = new List<Inventory>();
		invs.Add(inventory1);
		invs.Add(inventory2);

		working = invs[0];

		ItemStack stack1 = new ItemStack(Items.Gasdad,20);
		ItemStack stack2 = new ItemStack(Items.Gasdad,40);

		inventory1.Scatter(stack1,stack2);

		//inventory1.Sort();

		while (shouldRun) {
			for (int i = 0; i < invs.Count; i++) {
				Console.WriteLine("inventory " + i + (working == invs[i] ? ", working" : ""));
				Console.WriteLine(invs[i]);
			}
			Evaluate (Console.ReadLine());
			Console.WriteLine();
		}
	}

	static void Evaluate (string command) {
		command.ToLower();
		string[] tokens = command.Split(' ');
		if(tokens.Length < 1)
			return;
		if (tokens[0] == "exit") {
			shouldRun = false;
			return;
		}
		foreach (Command c in commands) {
			if (tokens[0] == c.key && tokens.Length-1 >= c.parameters) {
				c.Invoke(tokens);
				return;
			}
		}
	}
	static void InitCommands() {
		commands = new List<Command>();
		commands.Add(new Command(
			"transfer",
			(p) => { working.Transfer((int)p[0],(int)p[1],(int)p[2]); },
			3
		));
		commands.Add(new Command(
			"transferi",
			(p) => { invs[(int)p[0]].Transfer(invs[(int)p[2]],(int)p[1],(int)p[3],(int)p[4]); },
			5
		));
		commands.Add(new Command(
			"swap",
			(p) => { working.Swap((int)p[0],(int)p[1]); },
			2
		));
		commands.Add(new Command(
			"swapi",
			(p) => { invs[(int)p[0]].Swap(invs[(int)p[2]],(int)p[1],(int)p[3]); },
			4
		));
		commands.Add(new Command(
			"merge",
			(p) => { working.Merge((int)p[0],(int)p[1]); },
			2
		));
		commands.Add(new Command(
			"mergei",
			(p) => { invs[(int)p[0]].Merge(invs[(int)p[2]],(int)p[1],(int)p[3]); },
			4
		));
		commands.Add(new Command(
			"split",
			(p) => { working.Split((int)p[0],(int)p[1]); },
			2
		));
		commands.Add(new Command(
			"spliti",
			(p) => { invs[(int)p[0]].Split(invs[(int)p[2]],(int)p[1],(int)p[3]); },
			4
		));
		commands.Add(new Command(
			"working",
			(p) => { working = invs[(int)p[0]]; },
			1
		));
		commands.Add(new Command(
			"expand",
			(p) => { working.Expand((int)p[0]); },
			1
		));
		commands.Add(new Command(
			"contract",
			(p) => { working.Contract((int)p[0]); },
			1
		));
		commands.Add(new Command(
			"read",
			(p) => { Console.WriteLine(working[(int)p[0]]); },
			1
		));
		commands.Add(new Command(
			"invoke",
			(p) => {
				IActionable baseItem = working[(int)p[0]].ItemType as IActionable;
				if(baseItem != null)
					baseItem.Invoke((string)p[1],Knapsack.Knapsack.RequestData(working[(int)p[0]]));
			},
			2
		));
		commands.Add(new Command(
			"deposit",
			(p) => {
				BaseItem itemType = null;
				foreach(System.Reflection.FieldInfo field in typeof(Items).GetFields()) {
					if(field.FieldType.IsSubclassOf(typeof(BaseItem)) && field.Name.ToLower() == (string)p[0]) {
						itemType = (BaseItem)field.GetValue(null);
						break;
					}
				}
				working.Deposit<BaseItem> (new ItemStack(itemType,(int)p[1]));
			},
			2
		));
		commands.Add(new Command(
			"place",
			(p) => {
				BaseItem itemType = null;
				foreach(System.Reflection.FieldInfo field in typeof(Items).GetFields()) {
					if(field.FieldType.IsSubclassOf(typeof(BaseItem)) && field.Name.ToLower() == (string)p[0]) {
						itemType = (BaseItem)field.GetValue(null);
						break;
					}
				}
				working.Place(new ItemStack(itemType,(int)p[1]));
			},
			2
		));
		commands.Add(new Command(
			"insert",
			(p) => {
				BaseItem itemType = null;
				foreach(System.Reflection.FieldInfo field in typeof(Items).GetFields()) {
					if(field.FieldType.IsSubclassOf(typeof(BaseItem)) && field.Name.ToLower() == (string)p[0]) {
						itemType = (BaseItem)field.GetValue(null);
						break;
					}
				}
				working.Insert(new ItemStack(itemType,(int)p[1]),(int)p[2]);
			},
			3
		));
		commands.Add(new Command(
			"increase",
			(p) => { working.Increase((int)p[0],(int)p[1]); },
			2
		));
		commands.Add(new Command(
			"decrease",
			(p) => { working.Decrease((int)p[0],(int)p[1]); },
			2
		));
		commands.Add(new Command(
			"properties",
			(p) => {
				ItemStack stack = working[(int)p[0]];
				foreach(KeyValuePair<string,ItemProperty> entry in stack.Properties) {
					Console.WriteLine(string.Format("{0}: {1}",entry.Key,entry.Value));
				}
			},
			1
		));
		commands.Add(new Command(
			"damage",
			(p) => {
				BaseItem item = working[(int)p[0]].ItemType;
				if (item is IDamageable) {
					IDamageable damageable = (IDamageable)item;
					damageable.Damage(Knapsack.Knapsack.RequestData(working[(int)p[0]]), (int)p[1]);
				}
			},
			2
		));
		commands.Add(new Command(
			"equip",
			(p) => {
				ItemStack stack = working.Remove((int)p[0]);
				if(!(stack.ItemType is IEquippable) || !working.Place<BaseEquipment>(stack)) {
					working.Insert(stack,(int)p[0]);
				}
			},
			1
		));
		commands.Add(new Command(
			"unequip",
			(p) => {
				ItemStack stack = working.Remove((int)p[0]);
				if(working.SlotRestrictions[(int)p[0]] != typeof(BaseEquipment) || !working.Place(stack)) {
					working.Insert(stack,(int)p[0]);
				}
			},
			1
		));
		commands.Add(new Command(
			"equals",
			(p) => {
				// p[0] is slot 1
				// p[1] is slot 2
				ItemStack stack1 = working[(int)p[0]];
				ItemStack stack2 = working[(int)p[1]];

				Console.WriteLine("1 " + (stack1.Equivalent(stack2) ? "equals" : "does not equal") + " 2");
			},
			2
		));
		commands.Add(new Command(
			"sort",
			(p) => {
				working.Sort();
			},
			0
		));
	}
}
public struct Command {
	public string key;
	public Action<object[]> callback;
	public int parameters;

	public Command(string key, Action<object[]> callback, int parameters) {
		this.key = key;
		this.callback = callback;
		this.parameters = parameters;
	}

	public void Invoke(string[] tokens) {
		object[] parsed = new object[parameters];
		for (int i = 0; i < parameters; i++) {
			int toParseInt;
			if (int.TryParse(tokens[i+1], out toParseInt)) {
				parsed[i] = toParseInt;
				continue;
			}
			parsed[i] = tokens[i + 1];
		}
		callback(parsed);
	}
}