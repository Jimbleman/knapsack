﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace Knapsack {
	public class PropertyHolder : IEnumerable {
		protected Dictionary<string, ItemProperty> Properties;

		public PropertyHolder() {
			Properties = new Dictionary<string,ItemProperty>();
		}

		public PropertyHolder(Dictionary<string, ItemProperty> properties) {
			Properties = properties;
		}

		public bool Contains(string key) {
			return Properties.ContainsKey(key);
		}

		// Add<int>("name", 500)
		// Add<string>("name2", "other property")
		// Add<bool>("name3", true)
		// etc.
		public void Add<T>(string propertyName, T value) {
			if(!Properties.ContainsKey(propertyName)) {
				Properties.Add(propertyName,new ItemProperty<T>(value));
			}
		}

		public T Read<T>(string key) {
			ItemProperty property;
			if(Properties.TryGetValue(key,out property) && property is ItemProperty<T>) {
				return ((ItemProperty<T>)property).Value;
			}
			return default(T);
		}

		public object Read(string key) {
			ItemProperty property;
			if(Properties.TryGetValue(key,out property)) {
				return property.Get();
			}
			return null;
		}

		public void Write<T>(string key, T value) {
			if (Properties.ContainsKey(key) && Properties[key] is ItemProperty<T>) {
				((ItemProperty<T>)Properties[key]).Value = value;
			}
		}

		public int Count {
			get {
				return Properties.Count;
			}
		}

		public IEnumerator GetEnumerator() {
			return Properties.GetEnumerator();
		}

		public PropertyHolder Copy() {
			//PropertyHolder copy = new PropertyHolder();
			Dictionary<string,ItemProperty> copyProps = new Dictionary<string, ItemProperty>();
			foreach (KeyValuePair<string, ItemProperty> entry in Properties) {
				copyProps.Add(entry.Key,entry.Value.Copy());
			}
			return new PropertyHolder(copyProps);
		}

		public bool Equivalent(PropertyHolder other) {
			if(Count != other.Count)
				return false;
			foreach(KeyValuePair<string, ItemProperty> entry in other) {
				if(!Properties.ContainsKey(entry.Key))
					return false;
				if(!Properties[entry.Key].Equivalent(other.Properties[entry.Key]))
					return false;
			}
			return true;
		}
	}
	public class ItemProperty<T> : ItemProperty {
		T _value;
		public T Value {
			get {
				return _value;
			}
			set {
				_value = value;
			}
		}
		public ItemProperty (T value) {
			Value = value;
		}
		public override object Get() {
			return Value;
		}
		public override bool Equivalent(object other) {
			ItemProperty<T> otherG = other as ItemProperty<T>;
			if(otherG != null) {
				return Value.Equals(otherG.Value);
			} else {
				return false;
			}
		}
		public override ItemProperty Copy() {
			return new ItemProperty<T>(Value);
		}
		public static implicit operator T(ItemProperty<T> property) {
			return property.Value;
		}

		public override string ToString() {
			return Value.ToString();
		}
		public override int CompareTo(object other) {
			// as is is if not null
			ItemProperty<T> prop = other as ItemProperty<T>;
			if (prop != null) {
				T value = prop;
				return Comparer<T>.Default.Compare(Value,value);
			} else {
				return 1; // this is greater if other is not this
			}
		}
	}
	public abstract class ItemProperty : IComparable {
		public abstract object Get();

		public abstract bool Equivalent(object other);

		public abstract ItemProperty Copy();

		public abstract int CompareTo(object other);
	}
}